-- function get_suscribrer_data(ac)
--
-- 	printf(2,"obteniendo registro por primera vez")
-- 	smdb_connect()
-- 	SMQ= session:getVariable("prquery")
-- 	dbh:query(SMQ, function(srh_data)
--
-- 	printf(4,srh_data.sbr)
-- 		account = srh_data.sbr;
-- 		end)
--
-- 	if account == nil or account == "" then
--
--         if ac == nil then d = "notallowed"; else d = ac; end
-- 		session:execute("hash","insert/smarttelco/".. session:getVariable("sip_h_P-Core-SBC").."/"..d)
-- 		return d
-- 	else
-- 		session:execute("hash","insert/smarttelco/".. session:getVariable("sip_h_P-Core-SBC").."/"..account)
-- 		return account
--
-- 	end
--
-- end
--

-- ######### Consulta al WS ########

function get_suscribrer_data(ac)
	local json = require("json")
		printf(2,"obteniendo registro por primera vez")
	get_header = ' content-type application/JSON'
	url = session:getVariable("apipath") .. "/smc/get-auth?ip=" .. session:getVariable("sip_h_P-Core-SBC") .. "&alias=SMTVMD04"

	printf(4,"url = "..url)
	session:execute("curl", url .. get_header)
	response_code = session:getVariable("curl_response_code")
	printf(4, "response_code " .. response_code)

	if not response_code then return end

	if response_code == "200" then
		curl_response = session:getVariable("curl_response_data")
		json_response = json.decode(curl_response)
		printf(4,"Body = "..curl_response)
		if json_response.status == "OK" then
			session:execute("hash","insert/smarttelco/".. session:getVariable("sip_h_P-Core-SBC").."/"..curl_response)
			return curl_response
		else
			session:execute("hash","insert/smarttelco/".. session:getVariable("sip_h_P-Core-SBC").."/notallowed")
			return "notallowed"
		end
	else
		if ac == nil then d = "notallowed"; else d = ac; end
				session:execute("hash","insert/smarttelco/".. session:getVariable("sip_h_P-Core-SBC").."/"..d)
		return d
	end

end

function make_call()

	local json = require("json")
	local SBData, pos, msg = json.decode(SBR)

	if not SBData then
			printf(4, "Decode failed at "..tostring(pos)..": "..tostring(msg) )

	else

	act_tokens=freeswitch.getGlobalVariable("ACTIVE_TOKENS")
	if act_tokens == nil or act_tokens == "_undef_" or act_tokens=="" then act_tokens =0 else act_tokens = act_tokens+0 end
	token = freeswitch.getGlobalVariable(SBData.data.id)


	if token == nil or token == "" or token =="_undef_" then
			NW= act_tokens+1
			freeswitch.setGlobalVariable("TOKEN_"..NW,SBData.data.id)
			freeswitch.setGlobalVariable("ACTIVE_TOKENS",NW)
			freeswitch.setGlobalVariable(SBData.data.id,SBData.data.id)

	end


		session:setVariable("TOKEN",SBData.data.id );
		session:setVariable("remte_id",SBData.data.remte_id );
		session:setVariable("remote_ip",SBData.data.remote_ip );
		session:setVariable("remote_net",SBData.data.remote_net );
		session:setVariable("app_source",SBData.data.app_source );
		session:setVariable("media_on_answer",SBData.data.media_on_answer );
		session:setVariable("media_on_call",SBData.data.media_on_call );
		session:setVariable("ingress_prefix",SBData.data.ingress_prefix );
		session:setVariable("egress_prefix",SBData.data.egress_prefix );
		session:setVariable("max_channels",SBData.data.max_channels );
		session:setVariable("trunk_call",SBData.data.trunk_call);
		session:setVariable("another_profile",SBData.data.another_profile);
		session:setVariable("termination",SBData.data.termination_code);
		session:setVariable("enable_amd",SBData.data.amd);
		session:setVariable("buuid",api:executeString("create_uuid"));
		session:setVariable("RQ850",SBData.data.q850);
		session:setVariable("amd_reason",SBData.data.amd_cause);
		session:setVariable("ocid",SBData.data.cid);
		session:setVariable("strip",SBData.data.strip);
        session:setVariable("requested_token_epoch",SBData.data.requested_token_epoch)
        session:setVariable("expires_token_epoch",SBData.data.expires_token_epoch)
		session:setVariable("NDestination",string.sub(DST, SBData.data.strip+1))
		printf(3,session:getVariable("media_on_call"))


        if  session:getVariable("ocid") == '1' then
            session:setVariable("BCID",session:getVariable("sip_call_id"));
        else
        session:setVariable("BCID","SmartTELCO_"..session:getVariable("sip_call_id"));
        end

        if  session:getVariable("enable_amd") == '1' then
    		mediaTO = 2500
			session:execute("export", "nolocal:execute_on_answer_3=amd");
			session:execute("export", "nolocal:api_on_answer_2=bgapi lua  main.lua hdetection "..session:getVariable("buuid").." ".. session:getVariable("uuid"));
		else
			mediaTO = 2500

		end


		if session:getVariable("trunk_call") == '0' then
	   session:setVariable("dial_str","sofia/${sofia_profile_name}/${egress_prefix}${NDestination}@${network_addr}:${sip_received_port}");

--           session:setVariable("dial_str","sofia/${sofia_profile_name}/${egress_prefix}${NDestination}@${sip_h_P-Core-SBC}:" .. session:getVariable("port_in_use"));

		else
			session:setVariable("dial_str","sofia/gateway/${sip_h_P-Core-SBC}/${egress_prefix}${NDestination}");
		end



		if session:getVariable("media_on_call") == '0' then
		session:execute("export", "nolocal:api_on_answer_3=bgapi lua main.lua mediaout "..session:getVariable("uuid").." ".. mediaTO);

        end


	end
end


function refuse_call(reason)
	session:execute("hiredis_raw", "default INCR notallowed")
	session:setVariable("TOKEN",session:getVariable("sip_h_P-Core-SBC"));
	session:setVariable("CALL_TYPE","NOTALLOWED");
	session:execute("respond", "503 SmartVOICE: "..reason)
end

function condition_time()

    requested_epoch = session:getVariable("requested_token_epoch") + 0;
    expires_epoch = session:getVariable("expires_token_epoch") + 86400;
    actual_epoch = api:execute("strepoch") +0;

    printf(4, "{actual_epoch:"..actual_epoch..",requested_epoch:"..requested_epoch..",expires_epoch:"..expires_epoch.."}")

    re= requested_epoch + 86400;
    updated_token = session:getVariable("TOKEN").."_"..api:execute("strftime", "%Y%m%d")
    printf(1,updated_token)
    if (actual_epoch  >= re or actual_epoch >= expires_epoch ) and freeswitch.getGlobalVariable(updated_token) ~= "OK" then

        freeswitch.setGlobalVariable(updated_token, "OK")
        SBR=get_suscribrer_data(SBR);
        make_call();
        requested_epoch = session:getVariable("requested_token_epoch") + 0;
        expires_epoch = session:getVariable("expires_token_epoch") + 86400;
        printf(4, "{actual_epoch:"..actual_epoch..",requested_epoch:"..requested_epoch..",expires_epoch:"..expires_epoch.."}")
    end


    if actual_epoch >= expires_epoch then
        refuse_call("TOKEN VENCIDO");
    end

end

printf(2,"Iniciando proceso SVMD")
SBR = session:getVariable("account")
DST = session:getVariable("destination_number")
IPP = session:getVariable("port_in_use")

printf(2, "la respuesta es " .. IPP)
if SBR == nil or SBR == '_undef_' then
	SBR = get_suscribrer_data();
	print(4,"Token: "..SBR)
end

if SBR == "notallowed" then
		refuse_call("IP NOT ALLOWED");
else
	make_call();
    condition_time();
end
