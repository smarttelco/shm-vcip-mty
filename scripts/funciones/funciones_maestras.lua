-----------------------------------------------------------------------sts
function file_exists(fname)
   local f=io.open(fname,"r")
   if f~=nil then io.close(f) return true else return false end
end

-------------------------------------------------------------------------------

function printf(level,message)

        if level == 1 then   dbg="DEBUG";
        elseif level == 2 then  dbg="NOTICE";
        elseif level == 3 then  dbg="WARNING";
        elseif level == 4 then  dbg="ERR";
        end

        freeswitch.consoleLog(dbg,(message));
end

-------------------------------------------------------------------------------
-------------------------------------------------------------------------playurl
function play_back(playurl)

        session:execute("playback", (playurl))
end
-------------------------------------------------------------------------------
---------------------------------------------------------------fund_number_data

function find_number_data(nm_data)

        tmp_number = string.sub(nm_data, 0, -9)
        printf(1,tmp_number);

                if tmp_number == "55" or tmp_number == "33" or tmp_number == "81" then

                     tmp_nir = string.sub(nm_data, 0, -9)
                     tmp_num = string.sub(nm_data, 3)
                     tmp_serie = string.sub(nm_data, 3, -5)
                     tmp_nm = string.sub(nm_data, 7)

                else
                     tmp_nir = string.sub(nm_data, 0, -8)
                     tmp_num = string.sub(nm_data, 4)
                     tmp_serie = string.sub(nm_data, 4, -5)
                     tmp_nm = string.sub(nm_data, 7)

                end

end

function smdb_connect()

        dbh = freeswitch.Dbh("smdb", "postgres", "dbs0lt3cN3W")
        if dbh:connected() then
		printf(1,"conexion realizada");
		return true;
        else
		printf(4,"conexion denegada");
            return false;
        end

end

api = freeswitch.API()

function print_t (t,l)
    local print_r_cache={}
    local function sub_print_r(t,indent)
        if (print_r_cache[tostring(t)]) then
            printf(l, indent.."*"..tostring(t))
        else
            print_r_cache[tostring(t)]=true
            if (type(t)=="table") then
                for pos,val in pairs(t) do
                    if (type(val)=="table") then
                        printf(l, indent.."["..pos.."] => "..tostring(t).." {")
                        sub_print_r(val,indent..string.rep(" ",string.len(pos)+8))
                        printf(l, indent..string.rep(" ",string.len(pos)+6).."}")
                    elseif (type(val)=="string") then
                        printf(l, indent.."["..pos..'] => "'..val..'"')
                    else
                        printf(l, indent.."["..pos.."] => "..tostring(val))
                    end
                end
            else
                printf(l, indent..tostring(t))
            end
        end
    end
    if (type(t)=="table") then
        printf(l, tostring(t).." {")
        sub_print_r(t,"  ")
        printf(l, "}")
    else
        sub_print_r(t,"  ")
    end
    printf(l,"")
end

